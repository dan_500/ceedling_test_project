/* Includes */
#include "Termistor.h"
#include "stm32f0xx.h"
#include "Gpio.h"
#include "Adc.h"

/***************************************************************************************************
*  Local Macro Definitions
***************************************************************************************************/
/* Temperature sensor calibration value address */
#ifdef UNIT_TEST_BUILD /* This macro is only defined for the unit test build */
    extern uint16_t temp110_cal_addr;
    extern uint16_t temp30_cal_addr;
    #define TEMP110_CAL_ADDR (&temp110_cal_addr)
    #define TEMP30_CAL_ADDR (&temp30_cal_addr)
#else
    #define TEMP110_CAL_ADDR ((uint16_t*) ((uint32_t) 0x1FFFF7C2))
    #define TEMP30_CAL_ADDR ((uint16_t*) ((uint32_t) 0x1FFFF7B8))
#endif

#define VDD_CALIB ((uint16_t) (330))
#define VDD_APPLI ((uint16_t) (300))

/***************************************************************************************************
* Static Function Declarations
***************************************************************************************************/
static int32_t ComputeTemperature(uint32_t measure);

/***************************************************************************************************
* Function Definitions
***************************************************************************************************/
void Termistor_Conversion_MCAL(void)
{
    int32_t temperature_C; //contains the computed temperature

    while ((ADC1->ISR & ADC_ISR_EOC) == 0); /* Wait end of conversion */

    temperature_C = ComputeTemperature((int32_t) ADC1->DR);

    if (temperature_C > 45)
    {
      /* Report by switching on the orange led */
      GPIOC->BSRR = (1<<8); /* Set orange led on PC8 */
    }
    else
    {
      /* Stop to report the warning by switching off the orange led */
      GPIOC->BRR = (1<<8); //Switch off orange led on PC8
    }

    GPIOC->ODR ^= (1<<9); //Toggle green led on PC9

}

uint8_t Termistor_Conversion_HAL(void)
{
    int32_t temperature_C; // contains the computed temperature
    int32_t adc_val;
    uint8_t retval = E_OK;

    ADC_ISR_Wait_For_Conversion(ADC_CHANNEL_1);

    ADC_Get_Measurement(ADC_CHANNEL_1, &adc_val);

    temperature_C = ComputeTemperature(adc_val);

    if (temperature_C > 45)
    {
      /* Report by switching on the orange led */
      retval = GPIO_Set(GPIO_ON, GPIO_PC8); /* Set orange led on PC8 */
    }
    else
    {
      /* Stop to report the warning by switching off the orange led */
      retval = GPIO_Set(GPIO_OFF, GPIO_PC8); // Switch off orange led on PC8
    }

    retval = GPIO_Toggle(GPIO_PC9); // Toggle green led on PC9

    return retval;
}

/**
  * @brief  This function computes the temperature from the temperature sensor measure
  *         The computation uses the following formula :
  *         Temp = (Vsense - V30)/Avg_Slope + 30
  *         Avg_Slope = (V110 - V30) / (110 - 30)
  *         V30 = Vsense @30°C (calibrated in factory @3.3V)
  *         V110 = Vsense @110°C (calibrated in factory @3.3V)
  *         VDD_APPLI/VDD_CALIB coefficient allows to adapt the measured value
  *         according to the application board power supply versus the
  *         factory calibration power supply.
  * @param  measure is the a voltage measured from the temperature sensor (can have been filtered)
  * @retval returns the computed temperature
  */
static int32_t ComputeTemperature(uint32_t measure)
{
  int32_t temperature;

  temperature = ((measure * VDD_APPLI / VDD_CALIB) - (int32_t) *TEMP30_CAL_ADDR ) ;
  temperature = temperature * (int32_t)(110 - 30);
  temperature = temperature / (int32_t)(*TEMP110_CAL_ADDR - *TEMP30_CAL_ADDR);
  temperature = temperature + 30;
  return(temperature);
}