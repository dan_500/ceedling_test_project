#ifndef TERMISTOR_H
#define TERMISTOR_H

/* Support include for the source file main.c since this file doesn't have an .h file in the actual
 * project, it is needed to create this support header file since Ceedling needs an .h file to do
 * the inclusion/
 */

/* Header Includes */
#include <stdint.h>

#define E_OK       (0U)
#define E_NOT_OK   (1U)

/****************************************************************************************************
* Declarations
*****************************************************************************************************/
void Termistor_Conversion_MCAL(void);
uint8_t Termistor_Conversion_HAL(void);

#endif /* TERMISTOR_H */

