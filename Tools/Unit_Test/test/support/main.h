#ifndef MAIN_H
#define MAIN_H

/* Support include for the source file main.c since this file doesn't have an .h file in the actual
 * project, it is needed to create this support header file since Ceedling needs an .h file to do
 * the inclusion/
 */

/* Header Includes */

/****************************************************************************************************
* Declarations
*****************************************************************************************************/
int main2(void);

#endif /* MAIN_H */

