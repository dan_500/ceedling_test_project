#include "stm32f0xx.h"

void ADC_ISR_Wait_For_Conversion(uint8_t channel)
{
    (void)channel
};

void ADC_Get_Measurement(uint8_t channel, uint32_t* adc_val)
{
    (void)channel;
    (void)adc_val;
};