#ifndef ADC_H
#define ADC_H
#include <stdint.h>


void ADC_ISR_Wait_For_Conversion(uint8_t channel);
void ADC_Get_Measurement(uint8_t channel, uint32_t* adc_val);

#define ADC_CHANNEL_1  (1U)

#endif