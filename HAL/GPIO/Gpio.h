#ifndef GPIO_H
#define GPIO_H
#include <stdint.h>

uint8_t GPIO_Set(uint8_t status, uint8_t channel);
uint8_t GPIO_Toggle(uint8_t channel);

#define GPIO_ON    (0U)
#define GPIO_OFF   (1U)
#define GPIO_PC8   (8U)
#define GPIO_PC9   (9U)
#endif